<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

session_start();

require __DIR__ . '/../vendor/autoload.php';

$app = new \Slim\App([
	'settings' => [
		'displayErrorDetails' => true,
		'db' => [
			'driver' => 'mysql',
			'host' => 'localhost',
			'database' => 'codeground',
			'username' => 'root',
			'password' => 'QujerysaBe',
			'charset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix' => ''
		]
	],
]);

$container = $app->getContainer();

/*
 *	Container
 *	In the container we'll put all the dependency for the whole application
 *	(move it to another file, till to the // Middlewares area)
 */

// Database, Eloquent (illuminate/database)

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function($container) use($capsule) {
	return $capsule;
};

// View, Twig (slim/twig-view)

$container['view'] = function($container) {

	$view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
		'cache' => false
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$container->get('router'),
		$container->get('request')->getUri()
	));

	$view->getEnvironment()->addGlobal('auth', [
		'check' => $container->auth->check(),
		'user' => $container->auth->user()
	]);

	$filter = new Twig_SimpleFilter('user_hash', function ($string) {
    	return str_rot13($string);
	});

	$view->getEnvironment()->addFilter($filter);
	
	$view->getEnvironment()->addGlobal('flash', $container->flash);

	return $view;

};

// Validator, (respect/validation)

$container['validator'] = function($container) {
	return new App\Validation\Validator;
};

// Csrf, (slim/csrf) [ + Middleware in the bottom ]

$container['csrf'] = function($container) {
	return new \Slim\Csrf\Guard;
};

// Auth (custom: \App\Controllers\Auth + \App\Models\User)

$container['auth'] = function($container) {
	return new \App\Auth\Auth;
};

// Flash Message, (slim/flash)

$container['flash'] = function($container) {
	return new \Slim\Flash\Messages;
};

// Controllers (why?)

$container['HomeController'] = function($container) {
	return new App\Controllers\HomeController($container);
};

$container['CategoriesController'] = function($container) {
	return new App\Controllers\CategoriesController($container);
};

$container['UsersController'] = function($container) {
	return new App\Controllers\UsersController($container);
};

$container['BricksController'] = function($container) {
	return new App\Controllers\BricksController($container);
};

$container['AuthController'] = function($container) {
	return new App\Controllers\Auth\AuthController($container);
};


// Middlewares

$app->add(new \App\Middleware\ValidationErrorsMiddleware($container));
$app->add(new \App\Middleware\OldInputMiddleware($container));
$app->add(new \App\Middleware\CsrfViewMiddleware($container));

$app->add($container->csrf);

Respect\Validation\Validator::with('App\\Validation\\Rules');

require __DIR__ . '/../app/routes.php';