<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	protected $table = 'comments'; // Non Necessario!

	protected $fillable = [
		'user_id',
		'brick_id',
		'body'
	];
	
	public function user() {
 		return $this->belongsTo('App\Models\User');
 	}

}