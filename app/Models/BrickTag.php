<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrickTag extends Model {

	protected $table = 'bricks_tags'; // Non Necessario!

	protected $fillable = [
		'brick_id',
		'tag_id'
	];

	public function tag() {

		return $this->hasOne('\App\Models\Tag', 'id', 'tag_id');

	}

}