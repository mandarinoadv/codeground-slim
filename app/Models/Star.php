<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Star extends Model {

	protected $table = 'stars'; // Non Necessario!

	protected $fillable = [
		'user_id',
		'brick_id'
	];
	
	// public function user() {
 //        return $this->belongsTo('App\Models\User');
 //    }
	
	// public function category() {
	// 	return $this->belongsTo('App\Models\Category');
	// }

	// public function stars() {
	// 	return $this->hasManyThrough('App\Models\User', 'App\Models\Star');
	// }

}