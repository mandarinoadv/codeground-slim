<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrickUser extends Model {

	protected $table = 'bricks_users'; // Non Necessario!

	protected $fillable = [
		'brick_id',
		'user_id',
		'role'
	];

}