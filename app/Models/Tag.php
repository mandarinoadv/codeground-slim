<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	protected $table = 'tags'; // Non Necessario!

	protected $fillable = [
		'name'
	];
	
	// public function user() {
 //        return $this->belongsTo('App\Models\User');
 //    }
	
	// public function category() {
	// 	return $this->belongsTo('App\Models\Category');
	// }

	// public function stars() {
	// 	return $this->hasManyThrough('App\Models\User', 'App\Models\Star');
	// }

}