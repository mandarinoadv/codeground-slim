<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $table = 'users'; // Non Necessario!

	protected $fillable = [
		'email',
		'name',
		'password'
	];

	public function bricks() {
		return $this->hasMany('\App\Models\Brick')->where('private', 0);
	}
	
	public function bricks_mine() {
		return $this->hasMany('\App\Models\Brick');
	}

	
}