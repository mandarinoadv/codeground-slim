<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brick extends Model {

	protected $table = 'bricks'; // Non Necessario!

	protected $fillable = [
		'hash',
		'category_id',
		'user_id',
		'title',
		'description',
		'body',
		'private'
	];
	
	public function user() {
        return $this->belongsTo('App\Models\User');
    }
	
	public function category() {
		return $this->belongsTo('App\Models\Category');
	}

	public function stars() {
		return $this->hasMany('App\Models\Star');
	}

	public function comments() {
		return $this->hasMany('App\Models\Comment');
	}

	public function tags() {
		return $this->hasMany('App\Models\BrickTag');
	}
	
	public function bakeJson($user_id = false) {
		
		$this->user = $this->user()->first();
		$this->comments = $this->comments()->get();

		if($this->comments) {
			foreach($this->comments as $comment) 
				$comment->user = \App\Models\User::find($comment->user_id);
		}

		if($user_id)
			$this->starred = \App\Models\Star::where('user_id', $user_id)->where('brick_id', $this->id)->first();
		
		$this->stars = $this->stars()->get();
		
	}

}