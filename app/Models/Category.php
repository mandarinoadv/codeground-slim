<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'categories'; // Non Necessario!

	protected $fillable = [
		'slug',
		'name',
		'description',
		'color'
	];
	
	public function bricks() {
		
		return $this->hasMany('App\Models\Brick');
		
	}

	public function freeBricks() {
		return $this->hasMany('App\Models\Brick')->where('private', 0)->orderBy('created_at', 'desc');
	}
	
	public function hasPrivate($user) {
		return $this->hasMany('App\Models\Brick')->where('private', 1)->where('user_id', $user->id);
	}

}