<?php

# Home Page
$app->get('/', 'HomeController:index')->setName('home');

# Sign-Up
$app->get('/auth/signup', 'AuthController:getSignUp')->setName('auth.signup');
$app->post('/auth/signup', 'AuthController:postSignUp');

# Login
$app->get('/auth/signin', 'AuthController:getSignIn')->setName('auth.signin');
$app->post('/auth/signin', 'AuthController:postSignIn');

# Get a Brick public
$app->get('/hole/{hash}', 'BricksController:getHole')->setName('bricks.hole');

$app->group('', function($container) use ($container) {
	
	# Dashboard
	$this->get('/dashboard', 'HomeController:dashboard')->setName('dashboard');
	
	# Logout
	$this->get('/auth/signout', 'AuthController:getSignOut')->setName('auth.signout');
	
	# Categories
	$this->get('/category/{slug}', 'CategoriesController:getSingle')->setName('categories.single');
	$this->get('/categories/new', 'CategoriesController:getNew')->setName('categories.new');
	$this->post('/categories/new', 'CategoriesController:postNew')->setName('categories.new');
	$this->get('/categories', 'CategoriesController:getIndex')->setName('categories.home');
    
	# Brick
	$this->get('/brick/{id}', 'BricksController:getSingle')->setName('bricks.single')->add(new \App\Middleware\BrickExistsMiddleware($container));
	$this->get('/brick/raw/{id}', 'BricksController:getRaw')->setName('bricks.raw')->add(new \App\Middleware\BrickExistsMiddleware($container));
	$this->get('/bricks/new', 'BricksController:getNew')->setName('bricks.new');
	$this->post('/bricks/new', 'BricksController:postNew');
	$this->get('/bricks/private', 'BricksController:getPrivate')->setName('bricks.private');
	
	$this->get('/api/brick/{id}.json', 'BricksController:getApi')->add(new \App\Middleware\BrickExistsMiddleware($container))->setName('bricks.single.api');
	
	# Brick Action
	$this->post('/bricks/comment/{id}', 'BricksController:postComment')->setName('bricks.comment')->add(new \App\Middleware\BrickExistsMiddleware($container));
	$this->post('/api/bricks/comment/{id}', 'BricksController:postCommentApi')->setName('bricks.comment.api')->add(new \App\Middleware\BrickExistsMiddleware($container));
	$this->get('/bricks/remove-comment/{id}', 'BricksController:getRemoveComment')->setName('bricks.comment.remove');
	$this->get('/bricks/star/{id}', 'BricksController:getStar')->setName('bricks.star')->add(new \App\Middleware\BrickExistsMiddleware($container));;
	
	# Get an user profile
	$this->get('/user/{email}', 'UsersController:getProfile')->setName('user.profile');

    
})->add(new \App\Middleware\AuthUserMiddleware($container));