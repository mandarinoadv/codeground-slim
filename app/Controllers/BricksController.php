<?php

namespace App\Controllers;

use App\Models\Brick;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Star;
use App\Models\BrickTag;
use App\Models\BrickUser;
use App\Models\Comment;
use Respect\Exceptions\NestedValidationException;
use Respect\Validation\Validator as v;

class BricksController extends Controller {

	/* Api Methods */
	public function getApi($request, $response) {
		
		$this->brick->bakeJson($this->auth->user() ? $this->auth->user()->id : false);
		return $response->withJson($this->brick);
		
	}
	
	public function getPrivate($request, $response) {
		
		$title = 'Your private bricks';
		
		$bricks = Brick::where('user_id', $this->auth->user()->id)->where('private', 1)->orderBy('updated_at', 'desc');
		
		$filter_category = $request->getParam('category');
		if($filter_category) {
			$cat = Category::find($filter_category);
			if($cat) {
				$bricks->where('category_id', $filter_category);
				$title .= ' - ' . $cat->name;
			}
		}
		
		$bricks = $bricks->get();
		
		return $this->view->render($response, 'bricks/archive.twig', ['title' => $title, 'bricks' => $bricks]);
		
		
	}
	
	public function getSingle($request, $response) {

		$brick = $this->brick;

		if(Star::where('user_id', $this->auth->user()->id)->where('brick_id', $brick->id)->first())
			$brick->starred = true;

		return $this->view->render($response, 'bricks/single.twig', ['brick' => $brick]);

	}

	public function getHole($request, $response) {

		if(!$brick = Brick::where('hash', $request->getAttribute('hash'))->first()) {
			$this->flash->addMessage('error', 'Brick not found.');
			return $response->withRedirect($this->router->pathFor('home'));
		}

		if($this->auth->user())
			return $response->withRedirect($this->router->pathFor('bricks.single', ['id' => $brick->id]));

		if($brick->private == 1) {
			$this->flash->addMessage('error', 'You can\'t see this brick.');
			return $response->withRedirect($this->router->pathFor('home'));
		}
		
		return $this->view->render($response, 'bricks/single.twig', ['brick' => $brick, 'hole' => 1]);

	}

	public function getRaw($request, $response) {

		$brick = $this->brick;

		$body = $response->getBody();
		$body->write($brick->body);

		return $response->withBody($body)->withHeader('Content-Type', 'text/plain');

	}

	public function getNew($request, $response) {

		$categories = Category::all();

		return $this->view->render($response, 'bricks/new.twig', ['categories' => $categories]);

	}

	public function postNew($request, $response) {

		$validation = $this->validator->validate($request, [
			'category_id' => v::numeric()->min(1),
			'title' => v::notEmpty(),
			'body' => v::notEmpty(),
		]);

		if($validation->failed()) {
			
			if(isset($_SESSION['errors']['category_id']))
				$_SESSION['errors']['category_id'] = ['Pick a category'];

			return $response->withRedirect($this->router->pathFor('bricks.new'));
		}

		$tags = array_map(function($val) {
			return trim($val);
		}, explode(',', $request->getParam('tags')));

		$brick = Brick::create([
			'hash' => md5(time() * rand(0,2391) . md5(time())),
			'category_id' => $request->getParam('category_id'),
			'user_id' => $this->auth->user()->id,
			'title' => $request->getParam('title'),
			'description' => $request->getParam('description'),
			'body' => $request->getParam('body'),
			'private' => $request->getParam('private') ? 1 : 0
		]);

		foreach($tags as $tag) {

			$tag = Tag::firstOrCreate([
				'name' => $tag
			]);

			$association = BrickTag::create([
				'brick_id' => $brick->id,
				'tag_id' => $tag->id
			]);

		}

		BrickUser::create([
			'user_id' => $this->auth->user()->id,
			'role' => 100,
			'brick_id' => $brick->id
		]);

		return $response->withRedirect($this->router->pathFor('bricks.single', [ 'id' => $brick->id ]));

	}

	public function postComment($request, $response) {

		$brick = $this->brick;

		$validation = $this->validator->validate($request, [
			'body' => v::notEmpty()
		]);

		if($validation->failed()) {
			return $response->withRedirect($this->router->pathFor('bricks.single', ['id' => $brick->id]));
		}

		$comment = Comment::create([
			'brick_id' => $brick->id,
			'user_id' => $this->auth->user()->id,
			'body' => $request->getParam('body')
		]);

		$brick->touch();

		return $response->withRedirect($this->router->pathFor('bricks.single', ['id' => $brick->id]));

	}

	public function postCommentApi($request, $response) {

		$brick = $this->brick;

		$code = 200;
		$return = [
			'status' => 'OK',
			'csrf' => ['name' => $this->csrf->getTokenName(), 'value' => $this->csrf->getTokenValue()]
		];

		$validation = $this->validator->validate($request, [
			'body' => v::notEmpty()
		]);

		if($validation->failed()) {

			$return['status'] = 'KO';
			$return['errors'] = $_SESSION['errors'];
			$code = 400;

		} else {

			$comment = Comment::create([
				'brick_id' => $brick->id,
				'user_id' => $this->auth->user()->id,
				'body' => $request->getParam('body')
			]);


			$brick->touch();
		}

		return $response->withJson($return);

		#return $response->withJson(['status' => 'OK']);

	}

	public function getStar($request, $response) {

		$brick = $this->brick;

		$starred = Star::where('brick_id', $brick->id)->where('user_id', $this->auth->user()->id)->first();
		if(!$starred) {
			Star::create([
				'brick_id' => $brick->id,
				'user_id' => $this->auth->user()->id
			]);
		} else {
			$starred->delete();
		}

		$brick->touch();

		return $response->withRedirect($this->router->pathFor('bricks.single', ['id' => $brick->id]));

	}

	public function getRemoveComment($request, $response) {

		$id = $request->getAttribute('id');

		$comment = Comment::where('id', $id)->where('user_id', $this->auth->user()->id)->first();
		if(!$comment) return $response->withRedirect($this->router->pathFor('home'));

		$brickId = $comment->brick_id;
		
		$comment->delete();

		return $response->withRedirect($this->router->pathFor('bricks.single', ['id' => $brickId]));

	}

}