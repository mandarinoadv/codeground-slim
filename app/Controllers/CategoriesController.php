<?php

namespace App\Controllers;

use App\Models\Category;
use App\Models\Brick;
use Respect\Validation\Validator as v;

class CategoriesController extends Controller {

	public function getIndex($request, $response) {

		$categories = Category::all();
		return $this->view->render($response, 'categories/index.twig', ['categories' => $categories]);

	}
	
	public function getSingle($request, $response) {
		
		$category = Category::where('slug', $request->getAttribute('slug'))->first();
		
		if(!$category)
			return $response->withRedirect($this->router->pathFor('categories.home'));

		$bricks = $category->freeBricks;
		$hasPrivate = $category->hasPrivate($this->auth->user())->get();
		
		return $this->view->render($response, 'categories/single.twig', ['category' => $category, 'bricks' => $bricks, 'hasPrivate' => $hasPrivate]);
		
	}

	public function getNew($request, $response) {

		return $this->view->render($response, 'categories/new.twig');

	}

	public function postNew($request, $response) {

		$validation = $this->validator->validate($request, [
			'name' => v::notEmpty()->alpha()
		]);

		if($validation->failed())
			return $response->withRedirect($this->router->pathFor('categories.new'));

		$slug = \App\Support\Strings::slug($request->getParam('name'));

		if($already = Category::where('slug', $slug)->first()) {
			$_SESSION['errors']['Collision'] = ['There\'s already a Category with this slug: ' . $slug];
			return $response->withRedirect($this->router->pathFor('categories.new'));
		}

		$category = Category::create([
			'name' => $request->getParam('name'),
			'description' => $request->getParam('description'),
			'color' => $request->getParam('color'),
			'slug' => $slug,
		]);

		return $response->withRedirect($this->router->pathFor('categories.single', [ 'slug' => $slug ]));

	}

}