<?php

namespace App\Controllers;

use App\Models\User;

class UsersController extends Controller {

	public function getProfile($request, $response) {

		$user = User::where('email', $request->getAttribute('email'))->first();
		if(!$user)
			return $response->withRedirect($this->router->pathFor('home'));
		
		if($this->auth->check() && $this->auth->user()->id == $user->id)
			$bricks = $user->bricks_mine;
		else 
			$bricks = $user->bricks;
		
		return $this->view->render($response, 'users/profile.twig', ['user' => $user, 'bricks' => $bricks]);

	}
	
	public function getSingle($request, $response) {
		
		$category = Category::where('slug', $request->getAttribute('slug'))->first();
		
		$bricks = $category->bricks;
		
		return $this->view->render($response, 'categories/single.twig', ['category' => $category, 'bricks' => $bricks]);
		
	}

}