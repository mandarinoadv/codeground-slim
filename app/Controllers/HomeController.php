<?php

namespace App\Controllers;

use \App\Models\Brick;

class HomeController extends Controller {

	public function index($request, $response) {

		if($this->auth->user()) {
			return $response->withRedirect($this->router->pathFor('dashboard'));
		}

		return $this->view->render($response, 'home.twig', ['bricks' => $bricks]);

	}

	public function dashboard($request, $response) {

		$bricks = Brick::orderBy('updated_at', 'desc')->limit(5)->where('private', 0)->get();
		#echo '<pre>' . print_r($bricks, 1);
		return $this->view->render($response, 'dashboard.twig', ['bricks' => $bricks]);

	}

}