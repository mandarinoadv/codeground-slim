<?php

namespace App\Middleware;

use \App\Models\Brick;

class BrickExistsMiddleware extends Middleware {

	public function __invoke($request, $response, $next) {

		$route = $request->getAttribute('route');
		$brick_id = $route->getArgument('id');

		$brick = Brick::find($brick_id);

		if($brick && ($brick->private == 0  || $brick->user_id == $this->container->auth->user()->id)) {

			$this->container->brick = $brick;
			return $next($request, $response);

		} else {
			$this->container->flash->addMessage('error', 'You can\'t see this fucking brick.');
			return $response->withRedirect($this->container->router->pathFor('dashboard'));
		}

	}

}