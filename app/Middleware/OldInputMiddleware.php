<?php

namespace App\Middleware;

class OldInputMiddleware extends Middleware {

	/*
	 *	Anche questo Middleware è interessante perché aggiunge funzionalità allo strato `view` del container.
	 */

	public function __invoke($request, $response, $next) {

		$this->container->view->getEnvironment()->addGlobal('old', $_SESSION['old']);

		$_SESSION['old'] = $request->getParams();

		$response = $next($request, $response);
		return $response;

	}

}