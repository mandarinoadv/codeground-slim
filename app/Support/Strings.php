<?php

namespace App\Support;

class Strings {

	static function slug($phrase, $maxLength = 50) {
		
		$subs = array('à', 'á', 'è', 'é', 'ì', 'í', 'ò', 'ó', 'ù', 'ú');
		$rep  = array('a', 'a', 'e', 'e', 'i', 'i', 'o', 'o', 'u', 'u');

		$result = str_replace($subs, $rep, strtolower($phrase));
		
		$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
		$result = trim(preg_replace("/[\s-]+/", " ", $result));
		$result = trim(substr($result, 0, $maxLength));
		$result = preg_replace("/\s/", "-", $result);
		
		return $result;

	}

}